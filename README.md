Como posso alterar as definições de geolocalização?

Nas configurações do seu telemóvel.A primeira vez que se autenticar na nossa aplicação móvel, pediremos o seu consentimento para activar 
a geolocalização do seu dispositivo móvel. Se der o seu consentimento para a geolocalização do seu dispositivo móvel,
estas funções serão activadas imediatamente e funcionarão. Pode desactivar a geolocalização do seu dispositivo móvel em qualquer altura através das definições do seu telemóvel.



Quem tem acesso às minhas informações?

Todas as informações fornecidas são guardadas pelo Google Firebase e não são compartilhadas com nenhuma entidade.



A aplicação

A BATH RATE não é responsavel por nenhuma avaliação feita pelos usuarios, ficando claro que todas as avaliações são opiniões pontuais.
